import 'package:flutter/material.dart';
import 'package:test_task/models/result_data.dart';

import '../globals.dart' as globals;

class Result extends StatelessWidget {
  const Result({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResultDataTable();
  }
}

class ResultDataTable extends StatefulWidget {
  ResultDataTable({Key key}) : super(key: key);

  @override
  _ResultDataTableState createState() => _ResultDataTableState();
}

class _ResultDataTableState extends State<ResultDataTable> {
  _ResultDataSource _source = _ResultDataSource();
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  _ResultDataTableState() {
    getResultData().then((value) => setState(() {
          _source = _ResultDataSource.withData(value);
        }));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: PaginatedDataTable(
        rowsPerPage: _rowsPerPage,
        onRowsPerPageChanged: (value) {
          setState(() {
            _rowsPerPage = value;
          });
        },
        header: Text('Результаты расчета'),
        columns: [
          DataColumn(
            label: Text('Шаг'),
          ),
          DataColumn(
            label: Text('СКО.x'),
          ),
          DataColumn(
            label: Text('СКО.y'),
          ),
          DataColumn(
            label: Text('СКО.z'),
          ),
          DataColumn(
            label: Text('Среднее.x'),
          ),
          DataColumn(
            label: Text('Среднее.y'),
          ),
          DataColumn(
            label: Text('Среднее.z'),
          ),
        ],
        source: _source,
      ),
    );
  }
}

class _RowResult {
  _RowResult(
      {this.step,
      this.skoX,
      this.skoY,
      this.skoZ,
      this.meanX,
      this.meanY,
      this.meanZ});
  final int step;
  final double skoX;
  final double skoY;
  final double skoZ;
  final double meanX;
  final double meanY;
  final double meanZ;
}

class _ResultDataSource extends DataTableSource {
  List<_RowResult> _rowResults = [];
  _ResultDataSource();
  _ResultDataSource.withData(List<_RowResult> resultData) {
    _rowResults = resultData;
  }

  //set loadRowResults(List<_RowResult> v) => _rowResults = v;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rowResults.length) return null;
    final _rowResult = _rowResults[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${_rowResult.step}')),
        DataCell(Text('${_rowResult.skoX}')),
        DataCell(Text('${_rowResult.skoY}')),
        DataCell(Text('${_rowResult.skoZ}')),
        DataCell(Text('${_rowResult.meanX}')),
        DataCell(Text('${_rowResult.meanY}')),
        DataCell(Text('${_rowResult.meanZ}')),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _rowResults.length;

  @override
  int get selectedRowCount => 0;
}

Future<List<_RowResult>> getResultData() async {
  List<_RowResult> res = [];
  ChunkResult item;
  print('222');
  for (int i = 0; i < globals.finalResult.stepResult.length; i++) {
    item = globals.finalResult.stepResult[i];
    res.add(_RowResult(
      step: i,
      skoX: item.sko.x,
      skoY: item.sko.y,
      skoZ: item.sko.z,
      meanX: item.mean.x,
      meanY: item.mean.y,
      meanZ: item.mean.z,
    ));
  }
  print('333');
  return res;
}
// class Result extends StatefulWidget {
//   Result({Key key}) : super(key: key);

//   @override
//   _ResultState createState() => _ResultState();
// }

// class _ResultState extends State<Result> {
//   var dataTableSource = CreateDataTableSource();

//   _ResultState() {
//     getFinalReuslts().then((value) => setState(() {
//           print('555');
//         }));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.all(10.0),
//       margin: const EdgeInsets.symmetric(horizontal: 30.0),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(30.0),
//         color: Colors.white70,
//       ),
//       child: _finalResults,
//     );
//   }
// }

// Future<DataTable> getFinalReuslts() async {
//   return DataTable(columns: [
//     DataColumn(label: Text("Среднее")),
//     DataColumn(label: Text("СКО")),
//   ], rows: [
//     for (ChunkResult item in globals.finalResult.stepResult)
//       DataRow(cells: [
//         DataCell(Text(
//             '${item.main.x.toStringAsFixed(3)}  ${item.main.y.toStringAsFixed(3)}, ${item.main.z.toStringAsFixed(3)}')),
//         DataCell(Text(
//             '${item.sko.x.toStringAsFixed(3)}  ${item.sko.y.toStringAsFixed(3)}, ${item.sko.z.toStringAsFixed(3)}')),
//       ]),
//   ]);
// }
