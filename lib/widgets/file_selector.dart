import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import '../globals.dart' as globals;

class FileSelector extends StatelessWidget {
  final Function onGetFilePath;
  final filePath;
  FileSelector({Key key, this.onGetFilePath, this.filePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var txt = TextEditingController();
    txt.text = filePath;
    return Container(
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white70,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: TextField(
              controller: txt,
              obscureText: false,
              enabled: false, //Отключен ввод пользователя
              decoration: const InputDecoration(
                labelText: 'Файла',
              ),
            ),
          ),
          FlatButton(
            onPressed: () async {
              print('ddss');

              String path = await FilePicker.getFilePath();
              print(path);
              globals.filePath = path;
              onGetFilePath(path);
            },
            child: Text(
              'Загрузить',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
