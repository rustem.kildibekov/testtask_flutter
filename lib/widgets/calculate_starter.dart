//import 'dart:ffi';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:test_task/models/source_data.dart';
import 'package:test_task/models/result_data.dart';
import 'package:path_provider/path_provider.dart' as p;
import '../globals.dart' as globals;
import 'package:permission_handler/permission_handler.dart';

import '../models/source_data.dart';
import '../models/result_data.dart';
import 'dart:math';
import 'dart:io';
import 'dart:async';
//import 'package:typed_data/typed_data.dart';

//import '../main.dart';

class CalculateStater extends StatelessWidget {
  final Function onGetResult;
  final hasPath;
  const CalculateStater({Key key, this.onGetResult, this.hasPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.symmetric(horizontal: 30.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white70,
      ),
      child: FlatButton(
        onPressed: hasPath
            ? () => calculate(globals.filePath).then((_) {
                  calculateMeanAndSKO();
                  onGetResult();
                })
            : null,
        child: Text(
          'Обработать данные',
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
      ),
    );
  }
}

Future<void> calculate(String fileName) async {
  ByteData byteData = await readFile(fileName);
  double _x, _y, _z;
  int g1 = 16383;

  await [
    Permission.location,
    Permission.storage,
  ].request();

  // if (await Permission.accessMediaLocation.request().isGranted) {
  //   print('accessMediaLocation');
  // }
  // if (await Permission.storage.request().isGranted) {
  //   print('storage');
  // }

  // String directory =
  //     await p.getExternalStorageDirectory().then((value) => value.path);

  // print(directory);
  //var file = new File('$directory/download/vectors2.txt');
  //file.writeAsString("some");
  //var sink = file.openWrite();

  for (int i = 0; i < byteData.lengthInBytes - 5; i += 6) {
    _x = byteData.getInt16(i, Endian.little) / g1;
    _y = byteData.getInt16(i + 2, Endian.little) / g1;
    _z = byteData.getInt16(i + 4, Endian.little) / g1;
    globals.vectorData.add(Vector(x: _x, y: _y, z: _z));
    // sink.write(
    //     'x:${byteData.getInt16(i, Endian.little)} y:${byteData.getInt16(i + 2, Endian.little)} z:${byteData.getInt16(i + 4, Endian.little)} \n');
  }

  // sink.close();
}

Future<ByteData> readFile(String fileName) async {
  File file = new File(fileName);
  Uint8List values = await file.readAsBytes();
  return ByteData.view(values.buffer);
}

void calculateMeanAndSKO() {
  List<Vector> currentChunk = [];
  Vector _mean = Vector(x: 0, y: 0, z: 0);
  Vector _sko = Vector(x: 0, y: 0, z: 0);

  for (int i = 0; i < globals.vectorData.getChunkNum(); i++) {
    currentChunk = globals.vectorData.chunk(i);
    _mean = calculateMain(currentChunk);
    _sko = calculateSKOn(currentChunk, _mean);
    globals.finalResult.add = ChunkResult(mean: _mean, sko: _sko);
  }
}

Vector calculateMain(List<Vector> chunk) {
  Vector res = Vector(x: 0, y: 0, z: 0);
  for (int i = 0; i < chunk.length; i++) {
    res.x += chunk[i].x;
    res.y += chunk[i].y;
    res.z += chunk[i].z;
  }
  res.x /= chunk.length;
  res.y /= chunk.length;
  res.z /= chunk.length;
  return res;
}

Vector calculateSKOn(List<Vector> chunk, Vector main) {
  Vector res = Vector(x: 0, y: 0, z: 0);
  for (int i = 0; i < chunk.length; i++) {
    res.x += pow((chunk[i].x - main.x), 2);
    res.y += pow((chunk[i].y - main.y), 2);
    res.z += pow((chunk[i].z - main.z), 2);
  }
  res.x /= chunk.length;
  res.y /= chunk.length;
  res.z /= chunk.length;
  return res;
}
