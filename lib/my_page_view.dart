import 'package:flutter/material.dart';
import 'pages/result_page.dart';
import 'pages/home_page.dart';

class MyPageView extends StatefulWidget {
  MyPageView({Key key}) : super(key: key);

  @override
  _MyPageViewState createState() => _MyPageViewState();
}

class _MyPageViewState extends State<MyPageView> {
  PageController pageController = PageController();

  void _swipeToPage(int value) => setState(() {
        pageController.animateToPage(
          value,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
        print('animateToPage to $value');
      });

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pageController,
      scrollDirection: Axis.horizontal,
      physics: NeverScrollableScrollPhysics(),
      children: [
        HomePage(
          swipeToPage: _swipeToPage,
        ),
        ResultPage(
          swipeToPage: _swipeToPage,
        ),
      ],
    );
  }
}
