import 'package:flutter/material.dart';
import 'package:test_task/widgets/result.dart';

class ResultPage extends StatelessWidget {
  final swipeToPage;

  const ResultPage({Key key, this.swipeToPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.navigate_before),
          tooltip: 'Назад',
          onPressed: () {
            swipeToPage(0);
          },
        ),
        title: Text('Результаты'),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Colors.amber,
        ),
        child: Result(),
      ),
    );
  }
}
