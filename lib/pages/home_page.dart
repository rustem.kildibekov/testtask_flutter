import 'package:flutter/material.dart';
import 'package:test_task/widgets/calculate_starter.dart';
import 'package:test_task/widgets/file_selector.dart';

class HomePage extends StatefulWidget {
  final swipeToPage;
  HomePage({Key key, this.swipeToPage}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(swipeToPage: swipeToPage);
}

class _HomePageState extends State<HomePage> {
  bool _hasFilePath = false;
  String _filePath = '';
  final swipeToPage;
  _HomePageState({this.swipeToPage});

  void _onGetFilePath(String filePath) => setState(() {
        _hasFilePath = true;
        _filePath = filePath;
      });
  void _onGetResult() => setState(() {
        swipeToPage(1);
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Тестовое задание'),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Colors.amber,
        ),
        child: Column(
          /*
          / 1)Меню выбора пути к файлу - FileSelector
          / 2)Статус бар - FileLoadingStatusBar
          / 3)Кнопка запуска расчета - CalculateStarter
          / 4)Меню результатов - Result
          */
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            FileSelector(
              onGetFilePath: _onGetFilePath,
              filePath: _filePath,
            ),
            SizedBox(
              height: 20.0,
            ),
            //Text('Статус бар'),
            CalculateStater(
              onGetResult: _onGetResult,
              hasPath: _hasFilePath,
            ),
            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
