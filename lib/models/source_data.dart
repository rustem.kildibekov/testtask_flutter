import 'package:flutter/foundation.dart';

class Vector {
  double x;
  double y;
  double z;

  Vector({
    @required this.x,
    @required this.y,
    @required this.z,
  });
}

class VectorData {
  int chunkSize = 50;
  List<Vector> list = [];
  List<Vector> get data => list;
  add(Vector value) => list.add(value);

  int getChunkNum() => list.length - chunkSize - 1;
  List<Vector> chunk(int index) =>
      list.getRange(index, index + chunkSize).toList();
}
