import 'package:flutter/foundation.dart';
import 'source_data.dart';

class ChunkResult {
  Vector mean;
  Vector sko;

  ChunkResult({
    @required this.mean,
    @required this.sko,
  });
}

class FinalResult {
  List<ChunkResult> stepResult = [];
  set add(ChunkResult value) => stepResult.add(value);
  List<ChunkResult> get getStepResult => stepResult;
}
